from selenium import webdriver
import time
from bs4 import BeautifulSoup


driver = webdriver.Firefox()
url= "https://pokemondb.net/pokedex/bulbasaur"
driver.maximize_window()
driver.get(url)

time.sleep(5)
content = driver.page_source.encode('utf-8').strip()
soup = BeautifulSoup(content,"html.parser")
officials = soup.find('th', string="Height").next_element.next_element.next_element

# for entry in officials:
#     print(str(entry))

print(officials.text)


driver.quit()