# # Using readlines()
# file1 = open('./pokemonName.txt', 'r')
# Lines = file1.readlines()

# newPalString = ''

# for line in Lines:

#     newPalString = newPalString + 'INCBIN "gfx/pokemon/' + line.lower().strip() + '/front.gbcpal", middle_colors\n' +\
#     'INCLUDE "gfx/pokemon/' + line.lower().strip() + '/shiny.pal"\n'

# print(newPalString)
# # writing to file
# file1 = open('newPalEntry.txt', 'w')
# file1.write(newPalString)
# file1.close()

from os import walk

loopVar = 'y'

while loopVar == 'y' or loopVar == 'Y':

    region = input("What region would you like to generate?  ")

    _, _, filenames = next(walk('/home/devin/Documents/ancientplatinum/data/pokemon/base_stats/' + region))

    tupleList = []
    outputString = ''

    for file in filenames:
        size = len(file)
        currentMon = file[:size - 4]
        
        # Using readlines()
        file1 = open('/home/devin/Documents/ancientplatinum/data/pokemon/base_stats/' + region + '/' + file, 'r')
        Line = file1.readline()

        dexNumber = Line[8] + Line[9] + Line[10]

        palTuple = ('INCBIN "gfx/pokemon/' + region + '/' + currentMon + '/front.gbcpal", middle_colors\n' +\
        'INCLUDE "gfx/pokemon/' + region + '/' + currentMon + '/shiny.pal"\n', dexNumber)

        tupleList.append(palTuple)

    for item in sorted(tupleList, key=lambda x: x[1]):
        outputString = outputString + item[0]

    # writing to file
    file1 = open('newPalEntry' + region.title() + '.txt', 'w')
    file1.write(outputString)
    file1.close()

    loopVar = input("Would you like to add another region? (Y/n)\t") or "y"

