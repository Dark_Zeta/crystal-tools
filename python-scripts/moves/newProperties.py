# Using readlines()
file1 = open('./tempMoves.txt', 'r')
Lines = file1.readlines()

newAtkString = ''

for line in Lines:
    atkName = line.strip() + ','
    newAtkString = newAtkString + '\tmove ' + atkName.ljust(13) + ' EFFECT_NORMAL_HIT,         40, NORMAL,   100, 35,   0\n'

print(newAtkString)
# writing to file
file1 = open('newAtkProperties.txt', 'w')
file1.write(newAtkString)
file1.close()
