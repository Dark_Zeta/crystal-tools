# Using readlines()
file1 = open('./tempMoves.txt', 'r')
Lines = file1.readlines()

hexStart = input("What hex value would you like to start at? ")

count = int(hexStart, 16)
newAtkString = ''

for line in Lines:
    atkName = line.strip()
    newAtkString = newAtkString + "\tconst " + atkName.ljust(12) + " ; " + hex(count)[2:] + "\n"
    count += 1

# writing to file
file1 = open('newAtkConst.txt', 'w')
file1.write(newAtkString)
file1.close()
