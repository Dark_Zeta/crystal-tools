/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.0.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../ancientPlatTool/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.0.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    const uint offsetsAndSize[80];
    char stringdata0[1101];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs), len 
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 10), // "MainWindow"
QT_MOC_LITERAL(11, 29), // "on_tabWidget_2_currentChanged"
QT_MOC_LITERAL(41, 0), // ""
QT_MOC_LITERAL(42, 5), // "index"
QT_MOC_LITERAL(48, 19), // "pokemon_EntryChange"
QT_MOC_LITERAL(68, 19), // "baseStats_UpdateGUI"
QT_MOC_LITERAL(88, 20), // "baseStats_UpdateText"
QT_MOC_LITERAL(109, 8), // "readFile"
QT_MOC_LITERAL(118, 8), // "filename"
QT_MOC_LITERAL(127, 25), // "on_listWidget_itemChanged"
QT_MOC_LITERAL(153, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(170, 4), // "item"
QT_MOC_LITERAL(175, 35), // "on_baseStats_HPSpinBox_valueC..."
QT_MOC_LITERAL(211, 4), // "arg1"
QT_MOC_LITERAL(216, 39), // "on_baseStats_AttackSpinBox_va..."
QT_MOC_LITERAL(256, 40), // "on_baseStats_DefenseSpinBox_v..."
QT_MOC_LITERAL(297, 41), // "on_baseStats_SpAttackSpinBox_..."
QT_MOC_LITERAL(339, 42), // "on_baseStats_SpDefenseSpinBox..."
QT_MOC_LITERAL(382, 38), // "on_baseStats_SpeedSpinBox_val..."
QT_MOC_LITERAL(421, 34), // "on_baseStats_HPSlider_valueCh..."
QT_MOC_LITERAL(456, 5), // "value"
QT_MOC_LITERAL(462, 38), // "on_baseStats_AttackSlider_val..."
QT_MOC_LITERAL(501, 39), // "on_baseStats_DefenseSlider_va..."
QT_MOC_LITERAL(541, 40), // "on_baseStats_SpAttackSlider_v..."
QT_MOC_LITERAL(582, 41), // "on_baseStats_SpDefenseSlider_..."
QT_MOC_LITERAL(624, 37), // "on_baseStats_SpeedSlider_valu..."
QT_MOC_LITERAL(662, 33), // "on_baseStats_TypeCheckBox_cli..."
QT_MOC_LITERAL(696, 7), // "checked"
QT_MOC_LITERAL(704, 46), // "on_baseStats_TypeComboBox1_cu..."
QT_MOC_LITERAL(751, 46), // "on_baseStats_TypeComboBox2_cu..."
QT_MOC_LITERAL(798, 33), // "on_baseStats_ItemCheckBox_cli..."
QT_MOC_LITERAL(832, 46), // "on_baseStats_ItemComboBox1_cu..."
QT_MOC_LITERAL(879, 46), // "on_baseStats_ItemComboBox2_cu..."
QT_MOC_LITERAL(926, 22), // "on_radioButton_toggled"
QT_MOC_LITERAL(949, 24), // "on_radioButton_2_toggled"
QT_MOC_LITERAL(974, 24), // "on_radioButton_3_toggled"
QT_MOC_LITERAL(999, 10), // "themeCheck"
QT_MOC_LITERAL(1010, 26), // "on_file_OpenButton_clicked"
QT_MOC_LITERAL(1037, 30), // "on_settings_GotoButton_clicked"
QT_MOC_LITERAL(1068, 32) // "on_settings_ReturnButton_clicked"

    },
    "MainWindow\0on_tabWidget_2_currentChanged\0"
    "\0index\0pokemon_EntryChange\0"
    "baseStats_UpdateGUI\0baseStats_UpdateText\0"
    "readFile\0filename\0on_listWidget_itemChanged\0"
    "QListWidgetItem*\0item\0"
    "on_baseStats_HPSpinBox_valueChanged\0"
    "arg1\0on_baseStats_AttackSpinBox_valueChanged\0"
    "on_baseStats_DefenseSpinBox_valueChanged\0"
    "on_baseStats_SpAttackSpinBox_valueChanged\0"
    "on_baseStats_SpDefenseSpinBox_valueChanged\0"
    "on_baseStats_SpeedSpinBox_valueChanged\0"
    "on_baseStats_HPSlider_valueChanged\0"
    "value\0on_baseStats_AttackSlider_valueChanged\0"
    "on_baseStats_DefenseSlider_valueChanged\0"
    "on_baseStats_SpAttackSlider_valueChanged\0"
    "on_baseStats_SpDefenseSlider_valueChanged\0"
    "on_baseStats_SpeedSlider_valueChanged\0"
    "on_baseStats_TypeCheckBox_clicked\0"
    "checked\0on_baseStats_TypeComboBox1_currentIndexChanged\0"
    "on_baseStats_TypeComboBox2_currentIndexChanged\0"
    "on_baseStats_ItemCheckBox_clicked\0"
    "on_baseStats_ItemComboBox1_currentIndexChanged\0"
    "on_baseStats_ItemComboBox2_currentIndexChanged\0"
    "on_radioButton_toggled\0on_radioButton_2_toggled\0"
    "on_radioButton_3_toggled\0themeCheck\0"
    "on_file_OpenButton_clicked\0"
    "on_settings_GotoButton_clicked\0"
    "on_settings_ReturnButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       9,       // revision
       0,       // classname
       0,    0, // classinfo
      31,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    1,  200,    2, 0x08,    0 /* Private */,
       4,    0,  203,    2, 0x08,    2 /* Private */,
       5,    0,  204,    2, 0x08,    3 /* Private */,
       6,    0,  205,    2, 0x08,    4 /* Private */,
       7,    1,  206,    2, 0x08,    5 /* Private */,
       9,    1,  209,    2, 0x08,    7 /* Private */,
      12,    1,  212,    2, 0x08,    9 /* Private */,
      14,    1,  215,    2, 0x08,   11 /* Private */,
      15,    1,  218,    2, 0x08,   13 /* Private */,
      16,    1,  221,    2, 0x08,   15 /* Private */,
      17,    1,  224,    2, 0x08,   17 /* Private */,
      18,    1,  227,    2, 0x08,   19 /* Private */,
      19,    1,  230,    2, 0x08,   21 /* Private */,
      21,    1,  233,    2, 0x08,   23 /* Private */,
      22,    1,  236,    2, 0x08,   25 /* Private */,
      23,    1,  239,    2, 0x08,   27 /* Private */,
      24,    1,  242,    2, 0x08,   29 /* Private */,
      25,    1,  245,    2, 0x08,   31 /* Private */,
      26,    1,  248,    2, 0x08,   33 /* Private */,
      28,    1,  251,    2, 0x08,   35 /* Private */,
      29,    1,  254,    2, 0x08,   37 /* Private */,
      30,    1,  257,    2, 0x08,   39 /* Private */,
      31,    1,  260,    2, 0x08,   41 /* Private */,
      32,    1,  263,    2, 0x08,   43 /* Private */,
      33,    1,  266,    2, 0x08,   45 /* Private */,
      34,    1,  269,    2, 0x08,   47 /* Private */,
      35,    1,  272,    2, 0x08,   49 /* Private */,
      36,    0,  275,    2, 0x08,   51 /* Private */,
      37,    0,  276,    2, 0x08,   52 /* Private */,
      38,    0,  277,    2, 0x08,   53 /* Private */,
      39,    0,  278,    2, 0x08,   54 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::QString, QMetaType::QString,    8,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void, QMetaType::Bool,   27,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Bool,   27,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Bool,   27,
    QMetaType::Void, QMetaType::Bool,   27,
    QMetaType::Void, QMetaType::Bool,   27,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->on_tabWidget_2_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->pokemon_EntryChange(); break;
        case 2: _t->baseStats_UpdateGUI(); break;
        case 3: _t->baseStats_UpdateText(); break;
        case 4: { QString _r = _t->readFile((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 5: _t->on_listWidget_itemChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 6: _t->on_baseStats_HPSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_baseStats_AttackSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_baseStats_DefenseSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_baseStats_SpAttackSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_baseStats_SpDefenseSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_baseStats_SpeedSpinBox_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_baseStats_HPSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_baseStats_AttackSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->on_baseStats_DefenseSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_baseStats_SpAttackSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->on_baseStats_SpDefenseSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->on_baseStats_SpeedSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->on_baseStats_TypeCheckBox_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->on_baseStats_TypeComboBox1_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->on_baseStats_TypeComboBox2_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->on_baseStats_ItemCheckBox_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->on_baseStats_ItemComboBox1_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->on_baseStats_ItemComboBox2_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->on_radioButton_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->on_radioButton_2_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->on_radioButton_3_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 27: _t->themeCheck(); break;
        case 28: _t->on_file_OpenButton_clicked(); break;
        case 29: _t->on_settings_GotoButton_clicked(); break;
        case 30: _t->on_settings_ReturnButton_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.offsetsAndSize,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
qt_incomplete_metaTypeArray<qt_meta_stringdata_MainWindow_t

, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<QString, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<QListWidgetItem *, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<int, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<bool, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>, QtPrivate::TypeAndForceComplete<void, std::false_type>


>,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 31)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 31)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 31;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
