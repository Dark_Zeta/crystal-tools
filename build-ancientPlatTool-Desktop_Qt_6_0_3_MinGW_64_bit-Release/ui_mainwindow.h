/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <codeeditor.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen_Base_Directory;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QVBoxLayout *verticalLayout_12;
    QHBoxLayout *navigation_Layout;
    QPushButton *file_OpenButton;
    QSpacerItem *settings_Spacer;
    QPushButton *settings_GotoButton;
    QTabWidget *tabWidget;
    QWidget *pokemon_Tab;
    QHBoxLayout *horizontalLayout;
    QSplitter *pokemon_Splitter;
    QTreeView *pokemon_Tree;
    QTabWidget *pokemon_Container;
    QWidget *baseStats_Tab;
    QHBoxLayout *horizontalLayout_2;
    QTabWidget *baseStats_Container;
    QWidget *baseStats_GUITab;
    QHBoxLayout *horizontalLayout_4;
    QSplitter *splitter;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_5;
    QFrame *baseStats_StatsFrame;
    QVBoxLayout *verticalLayout_2;
    QFrame *baseStats_HPFrame;
    QVBoxLayout *verticalLayout_4;
    QLabel *baseStats_HPLabel;
    QFrame *baseStats_HPValueFrame;
    QHBoxLayout *horizontalLayout_12;
    QSpinBox *baseStats_HPSpinBox;
    QSlider *baseStats_HPSlider;
    QFrame *baseStats_AttackFrame;
    QVBoxLayout *verticalLayout_5;
    QLabel *baseStats_AttackLabel;
    QFrame *baseStats_AttackValueFrame;
    QHBoxLayout *horizontalLayout_11;
    QSpinBox *baseStats_AttackSpinBox;
    QSlider *baseStats_AttackSlider;
    QFrame *baseStats_DefenseFrame;
    QVBoxLayout *verticalLayout_6;
    QLabel *baseStats_DefenseLabel;
    QFrame *baseStats_DefenseValueFrame;
    QHBoxLayout *horizontalLayout_10;
    QSpinBox *baseStats_DefenseSpinBox;
    QSlider *baseStats_DefenseSlider;
    QFrame *baseStats_SpAttackFrame;
    QVBoxLayout *verticalLayout_7;
    QLabel *baseStats_SpAttackLabel;
    QFrame *baseStats_SpAttackValueFrame;
    QHBoxLayout *horizontalLayout_9;
    QSpinBox *baseStats_SpAttackSpinBox;
    QSlider *baseStats_SpAttackSlider;
    QFrame *baseStats_SpDefenseFrame;
    QVBoxLayout *verticalLayout_8;
    QLabel *baseStats_SpDefenseLabel;
    QFrame *baseStats_SpDefenseValueFrame;
    QHBoxLayout *horizontalLayout_8;
    QSpinBox *baseStats_SpDefenseSpinBox;
    QSlider *baseStats_SpDefenseSlider;
    QFrame *baseStats_SpeedFrame;
    QVBoxLayout *verticalLayout_9;
    QLabel *baseStats_SpeedLabel;
    QFrame *baseStats_SpeedValueFrame;
    QHBoxLayout *horizontalLayout_6;
    QSpinBox *baseStats_SpeedSpinBox;
    QSlider *baseStats_SpeedSlider;
    QFrame *baseStat_VariousFrame;
    QVBoxLayout *verticalLayout_3;
    QFrame *baseStats_TypeFrame;
    QVBoxLayout *verticalLayout_10;
    QFrame *baseStats_TypeTextFrame;
    QHBoxLayout *horizontalLayout_13;
    QLabel *baseStats_TypeLabel;
    QSpacerItem *baseStats_TypeTextSpacer;
    QCheckBox *baseStats_TypeCheckBox;
    QFrame *baseStats_TypeValueFrame;
    QHBoxLayout *horizontalLayout_14;
    QComboBox *baseStats_TypeComboBox1;
    QComboBox *baseStats_TypeComboBox2;
    QSpacerItem *baseStats_TypeSpacer;
    QFrame *baseStats_ItemFrame;
    QVBoxLayout *verticalLayout_13;
    QFrame *baseStats_ItemTextFrame;
    QHBoxLayout *horizontalLayout_19;
    QLabel *baseStats_ItemLabel;
    QSpacerItem *baseStats_ItemTextSpacer;
    QCheckBox *baseStats_ItemCheckBox;
    QFrame *baseStats_ItemValueFrame;
    QHBoxLayout *horizontalLayout_20;
    QComboBox *baseStats_ItemComboBox1;
    QComboBox *baseStats_ItemComboBox2;
    QSpacerItem *baseStats_ItemSpacer;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_7;
    QListWidget *baseStats_TMHMList;
    QWidget *baseStats_TextTab;
    QHBoxLayout *horizontalLayout_3;
    CodeEditor *baseStats_CodeEditor;
    QWidget *tab_2;
    QWidget *Moves;
    QWidget *settings_Page;
    QVBoxLayout *verticalLayout_11;
    QPushButton *settings_ReturnButton;
    QFrame *settings_Frame;
    QVBoxLayout *verticalLayout_14;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QSpacerItem *verticalSpacer;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(829, 628);
        actionOpen_Base_Directory = new QAction(MainWindow);
        actionOpen_Base_Directory->setObjectName(QString::fromUtf8("actionOpen_Base_Directory"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        verticalLayout_12 = new QVBoxLayout(page);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        navigation_Layout = new QHBoxLayout();
        navigation_Layout->setObjectName(QString::fromUtf8("navigation_Layout"));
        file_OpenButton = new QPushButton(page);
        file_OpenButton->setObjectName(QString::fromUtf8("file_OpenButton"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(file_OpenButton->sizePolicy().hasHeightForWidth());
        file_OpenButton->setSizePolicy(sizePolicy);
        file_OpenButton->setMinimumSize(QSize(36, 36));
        file_OpenButton->setMaximumSize(QSize(36, 36));
        file_OpenButton->setBaseSize(QSize(25, 25));
        file_OpenButton->setLayoutDirection(Qt::LeftToRight);

        navigation_Layout->addWidget(file_OpenButton);

        settings_Spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        navigation_Layout->addItem(settings_Spacer);

        settings_GotoButton = new QPushButton(page);
        settings_GotoButton->setObjectName(QString::fromUtf8("settings_GotoButton"));
        sizePolicy.setHeightForWidth(settings_GotoButton->sizePolicy().hasHeightForWidth());
        settings_GotoButton->setSizePolicy(sizePolicy);
        settings_GotoButton->setMinimumSize(QSize(36, 36));
        settings_GotoButton->setMaximumSize(QSize(36, 36));
        settings_GotoButton->setBaseSize(QSize(25, 25));
        settings_GotoButton->setLayoutDirection(Qt::LeftToRight);

        navigation_Layout->addWidget(settings_GotoButton);


        verticalLayout_12->addLayout(navigation_Layout);

        tabWidget = new QTabWidget(page);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setEnabled(false);
        pokemon_Tab = new QWidget();
        pokemon_Tab->setObjectName(QString::fromUtf8("pokemon_Tab"));
        horizontalLayout = new QHBoxLayout(pokemon_Tab);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pokemon_Splitter = new QSplitter(pokemon_Tab);
        pokemon_Splitter->setObjectName(QString::fromUtf8("pokemon_Splitter"));
        pokemon_Splitter->setOrientation(Qt::Horizontal);
        pokemon_Tree = new QTreeView(pokemon_Splitter);
        pokemon_Tree->setObjectName(QString::fromUtf8("pokemon_Tree"));
        pokemon_Tree->setMinimumSize(QSize(200, 0));
        pokemon_Tree->setMaximumSize(QSize(200, 16777215));
        pokemon_Tree->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        pokemon_Tree->setDragEnabled(false);
        pokemon_Splitter->addWidget(pokemon_Tree);
        pokemon_Container = new QTabWidget(pokemon_Splitter);
        pokemon_Container->setObjectName(QString::fromUtf8("pokemon_Container"));
        pokemon_Container->setMinimumSize(QSize(450, 0));
        baseStats_Tab = new QWidget();
        baseStats_Tab->setObjectName(QString::fromUtf8("baseStats_Tab"));
        horizontalLayout_2 = new QHBoxLayout(baseStats_Tab);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        baseStats_Container = new QTabWidget(baseStats_Tab);
        baseStats_Container->setObjectName(QString::fromUtf8("baseStats_Container"));
        baseStats_GUITab = new QWidget();
        baseStats_GUITab->setObjectName(QString::fromUtf8("baseStats_GUITab"));
        horizontalLayout_4 = new QHBoxLayout(baseStats_GUITab);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        splitter = new QSplitter(baseStats_GUITab);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        frame = new QFrame(splitter);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_5 = new QHBoxLayout(frame);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        baseStats_StatsFrame = new QFrame(frame);
        baseStats_StatsFrame->setObjectName(QString::fromUtf8("baseStats_StatsFrame"));
        baseStats_StatsFrame->setFrameShape(QFrame::StyledPanel);
        baseStats_StatsFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(baseStats_StatsFrame);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        baseStats_HPFrame = new QFrame(baseStats_StatsFrame);
        baseStats_HPFrame->setObjectName(QString::fromUtf8("baseStats_HPFrame"));
        baseStats_HPFrame->setFrameShape(QFrame::NoFrame);
        baseStats_HPFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(baseStats_HPFrame);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        baseStats_HPLabel = new QLabel(baseStats_HPFrame);
        baseStats_HPLabel->setObjectName(QString::fromUtf8("baseStats_HPLabel"));
        QFont font;
        font.setBold(true);
        baseStats_HPLabel->setFont(font);

        verticalLayout_4->addWidget(baseStats_HPLabel);

        baseStats_HPValueFrame = new QFrame(baseStats_HPFrame);
        baseStats_HPValueFrame->setObjectName(QString::fromUtf8("baseStats_HPValueFrame"));
        baseStats_HPValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_HPValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_12 = new QHBoxLayout(baseStats_HPValueFrame);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(0, 0, 0, 0);
        baseStats_HPSpinBox = new QSpinBox(baseStats_HPValueFrame);
        baseStats_HPSpinBox->setObjectName(QString::fromUtf8("baseStats_HPSpinBox"));
        baseStats_HPSpinBox->setMinimum(1);
        baseStats_HPSpinBox->setMaximum(255);
        baseStats_HPSpinBox->setValue(1);

        horizontalLayout_12->addWidget(baseStats_HPSpinBox);

        baseStats_HPSlider = new QSlider(baseStats_HPValueFrame);
        baseStats_HPSlider->setObjectName(QString::fromUtf8("baseStats_HPSlider"));
        baseStats_HPSlider->setStyleSheet(QString::fromUtf8(""));
        baseStats_HPSlider->setMinimum(1);
        baseStats_HPSlider->setMaximum(255);
        baseStats_HPSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_12->addWidget(baseStats_HPSlider);


        verticalLayout_4->addWidget(baseStats_HPValueFrame);


        verticalLayout_2->addWidget(baseStats_HPFrame);

        baseStats_AttackFrame = new QFrame(baseStats_StatsFrame);
        baseStats_AttackFrame->setObjectName(QString::fromUtf8("baseStats_AttackFrame"));
        baseStats_AttackFrame->setFrameShape(QFrame::NoFrame);
        baseStats_AttackFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(baseStats_AttackFrame);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        baseStats_AttackLabel = new QLabel(baseStats_AttackFrame);
        baseStats_AttackLabel->setObjectName(QString::fromUtf8("baseStats_AttackLabel"));
        baseStats_AttackLabel->setFont(font);

        verticalLayout_5->addWidget(baseStats_AttackLabel);

        baseStats_AttackValueFrame = new QFrame(baseStats_AttackFrame);
        baseStats_AttackValueFrame->setObjectName(QString::fromUtf8("baseStats_AttackValueFrame"));
        baseStats_AttackValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_AttackValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_11 = new QHBoxLayout(baseStats_AttackValueFrame);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        baseStats_AttackSpinBox = new QSpinBox(baseStats_AttackValueFrame);
        baseStats_AttackSpinBox->setObjectName(QString::fromUtf8("baseStats_AttackSpinBox"));
        baseStats_AttackSpinBox->setMinimum(1);
        baseStats_AttackSpinBox->setMaximum(255);
        baseStats_AttackSpinBox->setValue(1);

        horizontalLayout_11->addWidget(baseStats_AttackSpinBox);

        baseStats_AttackSlider = new QSlider(baseStats_AttackValueFrame);
        baseStats_AttackSlider->setObjectName(QString::fromUtf8("baseStats_AttackSlider"));
        baseStats_AttackSlider->setMinimum(1);
        baseStats_AttackSlider->setMaximum(255);
        baseStats_AttackSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_11->addWidget(baseStats_AttackSlider);


        verticalLayout_5->addWidget(baseStats_AttackValueFrame);


        verticalLayout_2->addWidget(baseStats_AttackFrame);

        baseStats_DefenseFrame = new QFrame(baseStats_StatsFrame);
        baseStats_DefenseFrame->setObjectName(QString::fromUtf8("baseStats_DefenseFrame"));
        baseStats_DefenseFrame->setFrameShape(QFrame::NoFrame);
        baseStats_DefenseFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(baseStats_DefenseFrame);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        baseStats_DefenseLabel = new QLabel(baseStats_DefenseFrame);
        baseStats_DefenseLabel->setObjectName(QString::fromUtf8("baseStats_DefenseLabel"));
        baseStats_DefenseLabel->setFont(font);

        verticalLayout_6->addWidget(baseStats_DefenseLabel);

        baseStats_DefenseValueFrame = new QFrame(baseStats_DefenseFrame);
        baseStats_DefenseValueFrame->setObjectName(QString::fromUtf8("baseStats_DefenseValueFrame"));
        baseStats_DefenseValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_DefenseValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_10 = new QHBoxLayout(baseStats_DefenseValueFrame);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        baseStats_DefenseSpinBox = new QSpinBox(baseStats_DefenseValueFrame);
        baseStats_DefenseSpinBox->setObjectName(QString::fromUtf8("baseStats_DefenseSpinBox"));
        baseStats_DefenseSpinBox->setMinimum(1);
        baseStats_DefenseSpinBox->setMaximum(255);
        baseStats_DefenseSpinBox->setValue(1);

        horizontalLayout_10->addWidget(baseStats_DefenseSpinBox);

        baseStats_DefenseSlider = new QSlider(baseStats_DefenseValueFrame);
        baseStats_DefenseSlider->setObjectName(QString::fromUtf8("baseStats_DefenseSlider"));
        baseStats_DefenseSlider->setMinimum(1);
        baseStats_DefenseSlider->setMaximum(255);
        baseStats_DefenseSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_10->addWidget(baseStats_DefenseSlider);


        verticalLayout_6->addWidget(baseStats_DefenseValueFrame);


        verticalLayout_2->addWidget(baseStats_DefenseFrame);

        baseStats_SpAttackFrame = new QFrame(baseStats_StatsFrame);
        baseStats_SpAttackFrame->setObjectName(QString::fromUtf8("baseStats_SpAttackFrame"));
        baseStats_SpAttackFrame->setFrameShape(QFrame::NoFrame);
        baseStats_SpAttackFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_7 = new QVBoxLayout(baseStats_SpAttackFrame);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        baseStats_SpAttackLabel = new QLabel(baseStats_SpAttackFrame);
        baseStats_SpAttackLabel->setObjectName(QString::fromUtf8("baseStats_SpAttackLabel"));
        baseStats_SpAttackLabel->setFont(font);

        verticalLayout_7->addWidget(baseStats_SpAttackLabel);

        baseStats_SpAttackValueFrame = new QFrame(baseStats_SpAttackFrame);
        baseStats_SpAttackValueFrame->setObjectName(QString::fromUtf8("baseStats_SpAttackValueFrame"));
        baseStats_SpAttackValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_SpAttackValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_9 = new QHBoxLayout(baseStats_SpAttackValueFrame);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        baseStats_SpAttackSpinBox = new QSpinBox(baseStats_SpAttackValueFrame);
        baseStats_SpAttackSpinBox->setObjectName(QString::fromUtf8("baseStats_SpAttackSpinBox"));
        baseStats_SpAttackSpinBox->setMinimum(1);
        baseStats_SpAttackSpinBox->setMaximum(255);
        baseStats_SpAttackSpinBox->setValue(1);

        horizontalLayout_9->addWidget(baseStats_SpAttackSpinBox);

        baseStats_SpAttackSlider = new QSlider(baseStats_SpAttackValueFrame);
        baseStats_SpAttackSlider->setObjectName(QString::fromUtf8("baseStats_SpAttackSlider"));
        baseStats_SpAttackSlider->setMinimum(1);
        baseStats_SpAttackSlider->setMaximum(255);
        baseStats_SpAttackSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_9->addWidget(baseStats_SpAttackSlider);


        verticalLayout_7->addWidget(baseStats_SpAttackValueFrame);


        verticalLayout_2->addWidget(baseStats_SpAttackFrame);

        baseStats_SpDefenseFrame = new QFrame(baseStats_StatsFrame);
        baseStats_SpDefenseFrame->setObjectName(QString::fromUtf8("baseStats_SpDefenseFrame"));
        baseStats_SpDefenseFrame->setFrameShape(QFrame::NoFrame);
        baseStats_SpDefenseFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_8 = new QVBoxLayout(baseStats_SpDefenseFrame);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        baseStats_SpDefenseLabel = new QLabel(baseStats_SpDefenseFrame);
        baseStats_SpDefenseLabel->setObjectName(QString::fromUtf8("baseStats_SpDefenseLabel"));
        baseStats_SpDefenseLabel->setFont(font);

        verticalLayout_8->addWidget(baseStats_SpDefenseLabel);

        baseStats_SpDefenseValueFrame = new QFrame(baseStats_SpDefenseFrame);
        baseStats_SpDefenseValueFrame->setObjectName(QString::fromUtf8("baseStats_SpDefenseValueFrame"));
        baseStats_SpDefenseValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_SpDefenseValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_8 = new QHBoxLayout(baseStats_SpDefenseValueFrame);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        baseStats_SpDefenseSpinBox = new QSpinBox(baseStats_SpDefenseValueFrame);
        baseStats_SpDefenseSpinBox->setObjectName(QString::fromUtf8("baseStats_SpDefenseSpinBox"));
        baseStats_SpDefenseSpinBox->setMinimum(1);
        baseStats_SpDefenseSpinBox->setMaximum(255);
        baseStats_SpDefenseSpinBox->setValue(1);

        horizontalLayout_8->addWidget(baseStats_SpDefenseSpinBox);

        baseStats_SpDefenseSlider = new QSlider(baseStats_SpDefenseValueFrame);
        baseStats_SpDefenseSlider->setObjectName(QString::fromUtf8("baseStats_SpDefenseSlider"));
        baseStats_SpDefenseSlider->setMinimum(1);
        baseStats_SpDefenseSlider->setMaximum(255);
        baseStats_SpDefenseSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_8->addWidget(baseStats_SpDefenseSlider);


        verticalLayout_8->addWidget(baseStats_SpDefenseValueFrame);


        verticalLayout_2->addWidget(baseStats_SpDefenseFrame);

        baseStats_SpeedFrame = new QFrame(baseStats_StatsFrame);
        baseStats_SpeedFrame->setObjectName(QString::fromUtf8("baseStats_SpeedFrame"));
        baseStats_SpeedFrame->setFrameShape(QFrame::NoFrame);
        baseStats_SpeedFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_9 = new QVBoxLayout(baseStats_SpeedFrame);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        baseStats_SpeedLabel = new QLabel(baseStats_SpeedFrame);
        baseStats_SpeedLabel->setObjectName(QString::fromUtf8("baseStats_SpeedLabel"));
        baseStats_SpeedLabel->setFont(font);

        verticalLayout_9->addWidget(baseStats_SpeedLabel);

        baseStats_SpeedValueFrame = new QFrame(baseStats_SpeedFrame);
        baseStats_SpeedValueFrame->setObjectName(QString::fromUtf8("baseStats_SpeedValueFrame"));
        baseStats_SpeedValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_SpeedValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_6 = new QHBoxLayout(baseStats_SpeedValueFrame);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        baseStats_SpeedSpinBox = new QSpinBox(baseStats_SpeedValueFrame);
        baseStats_SpeedSpinBox->setObjectName(QString::fromUtf8("baseStats_SpeedSpinBox"));
        baseStats_SpeedSpinBox->setMinimum(1);
        baseStats_SpeedSpinBox->setMaximum(255);
        baseStats_SpeedSpinBox->setValue(1);

        horizontalLayout_6->addWidget(baseStats_SpeedSpinBox);

        baseStats_SpeedSlider = new QSlider(baseStats_SpeedValueFrame);
        baseStats_SpeedSlider->setObjectName(QString::fromUtf8("baseStats_SpeedSlider"));
        baseStats_SpeedSlider->setMinimum(1);
        baseStats_SpeedSlider->setMaximum(255);
        baseStats_SpeedSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_6->addWidget(baseStats_SpeedSlider);


        verticalLayout_9->addWidget(baseStats_SpeedValueFrame);


        verticalLayout_2->addWidget(baseStats_SpeedFrame);


        horizontalLayout_5->addWidget(baseStats_StatsFrame);

        baseStat_VariousFrame = new QFrame(frame);
        baseStat_VariousFrame->setObjectName(QString::fromUtf8("baseStat_VariousFrame"));
        baseStat_VariousFrame->setFrameShape(QFrame::StyledPanel);
        baseStat_VariousFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_3 = new QVBoxLayout(baseStat_VariousFrame);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        baseStats_TypeFrame = new QFrame(baseStat_VariousFrame);
        baseStats_TypeFrame->setObjectName(QString::fromUtf8("baseStats_TypeFrame"));
        baseStats_TypeFrame->setFrameShape(QFrame::StyledPanel);
        baseStats_TypeFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_10 = new QVBoxLayout(baseStats_TypeFrame);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        baseStats_TypeTextFrame = new QFrame(baseStats_TypeFrame);
        baseStats_TypeTextFrame->setObjectName(QString::fromUtf8("baseStats_TypeTextFrame"));
        baseStats_TypeTextFrame->setFrameShape(QFrame::NoFrame);
        baseStats_TypeTextFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_13 = new QHBoxLayout(baseStats_TypeTextFrame);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        baseStats_TypeLabel = new QLabel(baseStats_TypeTextFrame);
        baseStats_TypeLabel->setObjectName(QString::fromUtf8("baseStats_TypeLabel"));
        baseStats_TypeLabel->setFont(font);

        horizontalLayout_13->addWidget(baseStats_TypeLabel);

        baseStats_TypeTextSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(baseStats_TypeTextSpacer);

        baseStats_TypeCheckBox = new QCheckBox(baseStats_TypeTextFrame);
        baseStats_TypeCheckBox->setObjectName(QString::fromUtf8("baseStats_TypeCheckBox"));

        horizontalLayout_13->addWidget(baseStats_TypeCheckBox);


        verticalLayout_10->addWidget(baseStats_TypeTextFrame);

        baseStats_TypeValueFrame = new QFrame(baseStats_TypeFrame);
        baseStats_TypeValueFrame->setObjectName(QString::fromUtf8("baseStats_TypeValueFrame"));
        baseStats_TypeValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_TypeValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_14 = new QHBoxLayout(baseStats_TypeValueFrame);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        baseStats_TypeComboBox1 = new QComboBox(baseStats_TypeValueFrame);
        baseStats_TypeComboBox1->setObjectName(QString::fromUtf8("baseStats_TypeComboBox1"));

        horizontalLayout_14->addWidget(baseStats_TypeComboBox1);

        baseStats_TypeComboBox2 = new QComboBox(baseStats_TypeValueFrame);
        baseStats_TypeComboBox2->setObjectName(QString::fromUtf8("baseStats_TypeComboBox2"));

        horizontalLayout_14->addWidget(baseStats_TypeComboBox2);


        verticalLayout_10->addWidget(baseStats_TypeValueFrame);

        baseStats_TypeSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(baseStats_TypeSpacer);


        verticalLayout_3->addWidget(baseStats_TypeFrame);

        baseStats_ItemFrame = new QFrame(baseStat_VariousFrame);
        baseStats_ItemFrame->setObjectName(QString::fromUtf8("baseStats_ItemFrame"));
        baseStats_ItemFrame->setFrameShape(QFrame::StyledPanel);
        baseStats_ItemFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_13 = new QVBoxLayout(baseStats_ItemFrame);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        baseStats_ItemTextFrame = new QFrame(baseStats_ItemFrame);
        baseStats_ItemTextFrame->setObjectName(QString::fromUtf8("baseStats_ItemTextFrame"));
        baseStats_ItemTextFrame->setFrameShape(QFrame::NoFrame);
        baseStats_ItemTextFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_19 = new QHBoxLayout(baseStats_ItemTextFrame);
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        baseStats_ItemLabel = new QLabel(baseStats_ItemTextFrame);
        baseStats_ItemLabel->setObjectName(QString::fromUtf8("baseStats_ItemLabel"));
        baseStats_ItemLabel->setFont(font);
        baseStats_ItemLabel->setFrameShape(QFrame::NoFrame);

        horizontalLayout_19->addWidget(baseStats_ItemLabel);

        baseStats_ItemTextSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(baseStats_ItemTextSpacer);

        baseStats_ItemCheckBox = new QCheckBox(baseStats_ItemTextFrame);
        baseStats_ItemCheckBox->setObjectName(QString::fromUtf8("baseStats_ItemCheckBox"));

        horizontalLayout_19->addWidget(baseStats_ItemCheckBox);


        verticalLayout_13->addWidget(baseStats_ItemTextFrame);

        baseStats_ItemValueFrame = new QFrame(baseStats_ItemFrame);
        baseStats_ItemValueFrame->setObjectName(QString::fromUtf8("baseStats_ItemValueFrame"));
        baseStats_ItemValueFrame->setFrameShape(QFrame::NoFrame);
        baseStats_ItemValueFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout_20 = new QHBoxLayout(baseStats_ItemValueFrame);
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        baseStats_ItemComboBox1 = new QComboBox(baseStats_ItemValueFrame);
        baseStats_ItemComboBox1->setObjectName(QString::fromUtf8("baseStats_ItemComboBox1"));

        horizontalLayout_20->addWidget(baseStats_ItemComboBox1);

        baseStats_ItemComboBox2 = new QComboBox(baseStats_ItemValueFrame);
        baseStats_ItemComboBox2->setObjectName(QString::fromUtf8("baseStats_ItemComboBox2"));

        horizontalLayout_20->addWidget(baseStats_ItemComboBox2);


        verticalLayout_13->addWidget(baseStats_ItemValueFrame);

        baseStats_ItemSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_13->addItem(baseStats_ItemSpacer);


        verticalLayout_3->addWidget(baseStats_ItemFrame);


        horizontalLayout_5->addWidget(baseStat_VariousFrame);

        splitter->addWidget(frame);
        groupBox = new QGroupBox(splitter);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMaximumSize(QSize(100, 16777215));
        horizontalLayout_7 = new QHBoxLayout(groupBox);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        baseStats_TMHMList = new QListWidget(groupBox);
        baseStats_TMHMList->setObjectName(QString::fromUtf8("baseStats_TMHMList"));

        horizontalLayout_7->addWidget(baseStats_TMHMList);

        splitter->addWidget(groupBox);

        horizontalLayout_4->addWidget(splitter);

        baseStats_Container->addTab(baseStats_GUITab, QString());
        baseStats_TextTab = new QWidget();
        baseStats_TextTab->setObjectName(QString::fromUtf8("baseStats_TextTab"));
        horizontalLayout_3 = new QHBoxLayout(baseStats_TextTab);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        baseStats_CodeEditor = new CodeEditor(baseStats_TextTab);
        baseStats_CodeEditor->setObjectName(QString::fromUtf8("baseStats_CodeEditor"));
        baseStats_CodeEditor->setLineWrapMode(QPlainTextEdit::NoWrap);

        horizontalLayout_3->addWidget(baseStats_CodeEditor);

        baseStats_Container->addTab(baseStats_TextTab, QString());

        horizontalLayout_2->addWidget(baseStats_Container);

        pokemon_Container->addTab(baseStats_Tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        pokemon_Container->addTab(tab_2, QString());
        pokemon_Splitter->addWidget(pokemon_Container);

        horizontalLayout->addWidget(pokemon_Splitter);

        tabWidget->addTab(pokemon_Tab, QString());
        Moves = new QWidget();
        Moves->setObjectName(QString::fromUtf8("Moves"));
        tabWidget->addTab(Moves, QString());

        verticalLayout_12->addWidget(tabWidget);

        stackedWidget->addWidget(page);
        settings_Page = new QWidget();
        settings_Page->setObjectName(QString::fromUtf8("settings_Page"));
        verticalLayout_11 = new QVBoxLayout(settings_Page);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        settings_ReturnButton = new QPushButton(settings_Page);
        settings_ReturnButton->setObjectName(QString::fromUtf8("settings_ReturnButton"));
        sizePolicy.setHeightForWidth(settings_ReturnButton->sizePolicy().hasHeightForWidth());
        settings_ReturnButton->setSizePolicy(sizePolicy);
        settings_ReturnButton->setMinimumSize(QSize(36, 36));
        settings_ReturnButton->setMaximumSize(QSize(36, 36));
        settings_ReturnButton->setBaseSize(QSize(25, 25));
        settings_ReturnButton->setLayoutDirection(Qt::LeftToRight);

        verticalLayout_11->addWidget(settings_ReturnButton);

        settings_Frame = new QFrame(settings_Page);
        settings_Frame->setObjectName(QString::fromUtf8("settings_Frame"));
        settings_Frame->setFrameShape(QFrame::StyledPanel);
        settings_Frame->setFrameShadow(QFrame::Raised);
        verticalLayout_14 = new QVBoxLayout(settings_Frame);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        radioButton = new QRadioButton(settings_Frame);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));

        verticalLayout_14->addWidget(radioButton);

        radioButton_2 = new QRadioButton(settings_Frame);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));

        verticalLayout_14->addWidget(radioButton_2);

        radioButton_3 = new QRadioButton(settings_Frame);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));

        verticalLayout_14->addWidget(radioButton_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer);


        verticalLayout_11->addWidget(settings_Frame);

        stackedWidget->addWidget(settings_Page);

        verticalLayout->addWidget(stackedWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(1);
        tabWidget->setCurrentIndex(0);
        pokemon_Container->setCurrentIndex(1);
        baseStats_Container->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionOpen_Base_Directory->setText(QCoreApplication::translate("MainWindow", "Open Base Directory", nullptr));
        file_OpenButton->setText(QString());
        settings_GotoButton->setText(QString());
        baseStats_HPLabel->setText(QCoreApplication::translate("MainWindow", "HP", nullptr));
        baseStats_AttackLabel->setText(QCoreApplication::translate("MainWindow", "Attack", nullptr));
        baseStats_DefenseLabel->setText(QCoreApplication::translate("MainWindow", "Defense", nullptr));
        baseStats_SpAttackLabel->setText(QCoreApplication::translate("MainWindow", "Special Attack", nullptr));
        baseStats_SpDefenseLabel->setText(QCoreApplication::translate("MainWindow", "Special Defense", nullptr));
        baseStats_SpeedLabel->setText(QCoreApplication::translate("MainWindow", "Speed", nullptr));
        baseStats_TypeLabel->setText(QCoreApplication::translate("MainWindow", "Types", nullptr));
        baseStats_TypeCheckBox->setText(QCoreApplication::translate("MainWindow", "Mono Type?", nullptr));
        baseStats_ItemLabel->setText(QCoreApplication::translate("MainWindow", "Items", nullptr));
        baseStats_ItemCheckBox->setText(QCoreApplication::translate("MainWindow", "No Item?", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "GroupBox", nullptr));
        baseStats_Container->setTabText(baseStats_Container->indexOf(baseStats_GUITab), QCoreApplication::translate("MainWindow", "GUI", nullptr));
        baseStats_Container->setTabText(baseStats_Container->indexOf(baseStats_TextTab), QCoreApplication::translate("MainWindow", "Text", nullptr));
        pokemon_Container->setTabText(pokemon_Container->indexOf(baseStats_Tab), QCoreApplication::translate("MainWindow", "Base Stats", nullptr));
        pokemon_Container->setTabText(pokemon_Container->indexOf(tab_2), QCoreApplication::translate("MainWindow", "Tab 2", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(pokemon_Tab), QCoreApplication::translate("MainWindow", "Pokemon", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Moves), QCoreApplication::translate("MainWindow", "Moves", nullptr));
        settings_ReturnButton->setText(QString());
        radioButton->setText(QCoreApplication::translate("MainWindow", "RadioButton", nullptr));
        radioButton_2->setText(QCoreApplication::translate("MainWindow", "RadioButton", nullptr));
        radioButton_3->setText(QCoreApplication::translate("MainWindow", "RadioButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
