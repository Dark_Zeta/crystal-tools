#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileSystemModel>
#include <QItemSelection>
#include <QItemSelectionModel>
#include <QJsonObject>
#include <QJsonValue>
#include <QSettings>
#include <QTextStream>
#include <QToolTip>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QColor darkGray(53, 53, 53);
    QColor gray(128, 128, 128);
    QColor black(25, 25, 25);
    QColor blue(42, 130, 218);

    darkPalette.setColor(QPalette::Window, darkGray);
    darkPalette.setColor(QPalette::WindowText, Qt::white);
    darkPalette.setColor(QPalette::Base, black);
    darkPalette.setColor(QPalette::AlternateBase, darkGray);
    darkPalette.setColor(QPalette::ToolTipBase, blue);
    darkPalette.setColor(QPalette::ToolTipText, Qt::white);
    darkPalette.setColor(QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Button, darkGray);
    darkPalette.setColor(QPalette::ButtonText, Qt::white);
    darkPalette.setColor(QPalette::Link, blue);
    darkPalette.setColor(QPalette::Highlight, blue);
    darkPalette.setColor(QPalette::HighlightedText, Qt::white);

    darkPalette.setColor(QPalette::Active, QPalette::Button, gray.darker());
    darkPalette.setColor(QPalette::Inactive, QPalette::Text, Qt::white);
    darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, gray);
    darkPalette.setColor(QPalette::Disabled, QPalette::WindowText, gray);
    darkPalette.setColor(QPalette::Disabled, QPalette::Text, gray);
    darkPalette.setColor(QPalette::Disabled, QPalette::Light, darkGray);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_tabWidget_2_currentChanged(int index)
{
    if (index == 0)
    {
        switchBaseState();
    }
}

void MainWindow::switchBaseState()
{
    QString tabPath = dir + "/data/pokemon/base_stats";

    model->setFilter(QDir::NoDotAndDotDot
                     | QDir::AllDirs
                     | QDir::Files);

    model->setRootPath(tabPath);

    // Attach the model to the view
    ui->pokemon_Tree->setModel(model);

    ui->pokemon_Tree->setHeaderHidden(true);

    // first column is the name
    for (int i = 1; i < model->columnCount(); ++i)
        ui->pokemon_Tree->hideColumn(i);

    ui->pokemon_Tree->setRootIndex(model->index(tabPath));

    connect(ui->pokemon_Tree->selectionModel(), &QItemSelectionModel::selectionChanged, this, &MainWindow::pokemon_EntryChange);
}

void MainWindow::pokemon_EntryChange()
{
    baseStats_UpdateText();

    if (!ui->baseStats_CodeEditor->document()->isEmpty())
        baseStats_UpdateGUI();
}

void MainWindow::baseStats_UpdateGUI()
{
    ui->baseStats_ItemComboBox1->clear();
    ui->baseStats_ItemComboBox2->clear();

    QString itemString = readFile(dir + "/constants/item_constants.asm");
    QStringList itemStringList = itemString.split("\n");

    foreach (const QString &str, itemStringList)
    {
        QString itemCheck = str.split(" ").at(0);

        if (itemCheck == "\tconst")
        {
            ui->baseStats_ItemComboBox1->addItem(str.split(" ").at(1));
            ui->baseStats_ItemComboBox2->addItem(str.split(" ").at(1));
        }
    }

    int monItemLine = findLine(" ; items");
    QString monItemString = ui->baseStats_CodeEditor->document()->findBlockByLineNumber(monItemLine).text();

    monItemString.remove(" ; items");
    monItemString.remove("\tdb");
    monItemString.remove(" ");

    QStringList monItemList = monItemString.split(",");

    int item1Index = ui->baseStats_ItemComboBox1->findText(monItemList.at(0));
    if ( item1Index != -1 ) // -1 for not found
        ui->baseStats_ItemComboBox1->setCurrentIndex(item1Index);

    int item2Index = ui->baseStats_ItemComboBox2->findText(monItemList.at(1));
    if ( item2Index != -1 ) // -1 for not found
        ui->baseStats_ItemComboBox2->setCurrentIndex(item2Index);

    if (item1Index == 0 && item2Index == 0)
        ui->baseStats_ItemCheckBox->setChecked(true);
    else
        ui->baseStats_ItemCheckBox->setChecked(false);


    ui->baseStats_TypeComboBox1->clear();
    ui->baseStats_TypeComboBox2->clear();

    QString typeString = readFile(dir + "/constants/type_constants.asm");
    QStringList typeStringList = typeString.split("\n");

    foreach (const QString &str, typeStringList)
    {
        QString typeCheck = str.split(" ").at(0);

        if (typeCheck == "\tconst")
        {
            ui->baseStats_TypeComboBox1->addItem(str.split(" ").at(1));
            ui->baseStats_TypeComboBox2->addItem(str.split(" ").at(1));
        }
    }

    int monTypeLine = findLine(" ; type");
    QString monTypeString = ui->baseStats_CodeEditor->document()->findBlockByLineNumber(monTypeLine).text();

    monTypeString.remove(" ; type");
    monTypeString.remove("\tdb");
    monTypeString.remove(" ");

    QStringList monTypeList = monTypeString.split(",");

    int type1Index = ui->baseStats_TypeComboBox1->findText(monTypeList.at(0));
    if ( type1Index != -1 ) // -1 for not found
        ui->baseStats_TypeComboBox1->setCurrentIndex(type1Index);

    int type2Index = ui->baseStats_TypeComboBox2->findText(monTypeList.at(1));
    if ( type2Index != -1 ) // -1 for not found
        ui->baseStats_TypeComboBox2->setCurrentIndex(type2Index);

    if (type1Index == type2Index)
        ui->baseStats_TypeCheckBox->setChecked(true);
    else
        ui->baseStats_TypeCheckBox->setChecked(false);


    int monStatsLine = findLine(";   hp");
    QString monStatsString = ui->baseStats_CodeEditor->document()->findBlockByLineNumber(monStatsLine - 1).text();

    monStatsString.remove("\tdb");
    monStatsString.remove(" ");

    QStringList monStatsList = monStatsString.split(",");

    ui->baseStats_HPSpinBox->setValue(monStatsList.at(0).toInt());
    ui->baseStats_AttackSpinBox->setValue(monStatsList.at(1).toInt());
    ui->baseStats_DefenseSpinBox->setValue(monStatsList.at(2).toInt());
    ui->baseStats_SpAttackSpinBox->setValue(monStatsList.at(4).toInt());
    ui->baseStats_SpDefenseSpinBox->setValue(monStatsList.at(5).toInt());
    ui->baseStats_SpeedSpinBox->setValue(monStatsList.at(3).toInt());



    ui->baseStats_TMHMList->clear();

    QString moveMachineString = readFile(dir + "/constants/item_constants.asm");
    QStringList moveMachineStringList = moveMachineString.split("\n");

    int monTMHMLine = findLine("tmhm");
    QString monTMHMString = ui->baseStats_CodeEditor->document()->findBlockByLineNumber(monTMHMLine).text();

    monTMHMString.remove("\ttmhm");
    monTMHMString.remove(" ");

    QStringList monTMHMList = monTMHMString.split(",");

    foreach (const QString &str, moveMachineStringList)
    {
        QString moveMachineCheck = str.split(" ").at(0);

        if (moveMachineCheck == "\tadd_tm" || moveMachineCheck == "\tadd_hm")
        {
            QString moveMachineEntry = str.split(" ").at(1);
            QListWidgetItem * moveMachineItem = new QListWidgetItem(moveMachineEntry);

            if (monTMHMList.contains(moveMachineEntry))
                moveMachineItem->setCheckState(Qt::Checked);
            else
                moveMachineItem->setCheckState(Qt::Unchecked);

            ui->baseStats_TMHMList->addItem(moveMachineItem);
        }
    }
}

void MainWindow::baseStats_UpdateText()
{
    //    ui->label->setText(model->filePath(ui->pokemon_Tree->selectionModel()->currentIndex()));

    ui->baseStats_CodeEditor->clear();

    QString pokeBaseStatsText = readFile(model->filePath(ui->pokemon_Tree->selectionModel()->currentIndex()));

    ui->baseStats_CodeEditor->document()->setPlainText(pokeBaseStatsText);
}

QString MainWindow::readFile(QString filename)
{
    QFile file(filename);

    QString line = "";

    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        while (!stream.atEnd()){
            line = line + stream.readLine() + "\n";
        }
    }

    file.close();

    return line;
}

void MainWindow::setConstants(QString rootDir)
{
    QFile file(rootDir + "/constants.asm");

    QStringList files = {};

    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream in(&file);
        while (!in.atEnd()){
            QString line = in.readLine();
            if (line != "")
                files.append(rootDir + "/" + line.remove('\"').split(" ").at(1));
        }
    }

    file.close();

    QString fileList = "";

    foreach (QString str, files)
    {
//        fileList = fileList + str + "\n";
        QFile file(str);

        QStringList files = {};

        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream in(&file);
            while (!in.atEnd())
            {
                QString line = in.readLine();

                if (line != "" || !line.startsWith(";"))
                {
                    if (line.remove("\t").split(" ").at(0) == "charmap")
                    {
                        QString CMString = line.split(" ").at(1);
                        if (CMString.startsWith("\"<") && CMString.endsWith(">\",")) {
                            keywordPatterns.append(CMString.mid(1, CMString.size() - 3));
                        }
//                    fileList = fileList + line.remove("\t").split(" ").at(0) + "\n";
                    }
                }
            }
        }

        file.close();
    }

    keywordPatterns.append("hello");

    ui->baseStats_CodeEditor->document()->setPlainText(keywordPatterns[0]);
}

void MainWindow::setupEditor()
{
    QFont font;
    font.setFamily("Courier");
    font.setFixedPitch(true);
    font.setPointSize(10);

    ui->baseStats_CodeEditor->setFont(font);

    highlighter = new Highlighter(ui->baseStats_CodeEditor->document(), keywordPatterns);
}

bool MainWindow::verifyChange(QString message)
{
    QMessageBox::StandardButton alert;
    alert = QMessageBox::question(this, "Caution", message,
                                  QMessageBox::Yes|QMessageBox::No);

    if (alert == QMessageBox::Yes)
    {
        return true;
    }

    else
        return false;

}

void MainWindow::on_listWidget_itemChanged(QListWidgetItem *item)
{
    verifyChange("Hi there. :)");
}

int MainWindow::findLine(QString param)
{
    QStringList codeList = ui->baseStats_CodeEditor->toPlainText().split("\n");

    int retLine = codeList.indexOf(codeList.filter(param).at(0));
    return retLine;
}

void MainWindow::on_baseStats_HPSpinBox_valueChanged(int arg1)
{
    funcSlider(arg1, ui->baseStats_HPSlider);
}

void MainWindow::on_baseStats_AttackSpinBox_valueChanged(int arg1)
{
    funcSlider(arg1, ui->baseStats_AttackSlider);
}

void MainWindow::on_baseStats_DefenseSpinBox_valueChanged(int arg1)
{
    funcSlider(arg1, ui->baseStats_DefenseSlider);
}

void MainWindow::on_baseStats_SpAttackSpinBox_valueChanged(int arg1)
{
    funcSlider(arg1, ui->baseStats_SpAttackSlider);
}

void MainWindow::on_baseStats_SpDefenseSpinBox_valueChanged(int arg1)
{
    funcSlider(arg1, ui->baseStats_SpDefenseSlider);
}

void MainWindow::on_baseStats_SpeedSpinBox_valueChanged(int arg1)
{
    funcSlider(arg1, ui->baseStats_SpeedSlider);
}

void MainWindow::funcSlider(int val, QSlider* curSlider)
{
    curSlider->setValue(val);

    QPalette p;

    QColor veryLow(243, 68, 68);
    QColor low(255, 127, 15);
    QColor slightlyLow(255, 221, 87);
    QColor slightlyHigh(160, 229, 21);
    QColor high(35, 205, 94);
    QColor veryHigh(0, 194, 184);

    switch (curSlider->value())
    {
    case 1 ... 29:
        p.setColor(QPalette::Highlight, veryLow);
        break;
    case 30 ... 59:
        p.setColor(QPalette::Highlight, low);
        break;
    case 60 ... 89:
        p.setColor(QPalette::Highlight, slightlyLow);
        break;
    case 90 ... 119:
        p.setColor(QPalette::Highlight, slightlyHigh);
        break;
    case 120 ... 149:
        p.setColor(QPalette::Highlight, high);
        break;
    default:
        p.setColor(QPalette::Highlight, veryHigh);
        break;
    }

    curSlider->setPalette(p);
}

void MainWindow::on_baseStats_HPSlider_valueChanged(int value)
{
    ui->baseStats_HPSpinBox->setValue(value);
}

void MainWindow::on_baseStats_AttackSlider_valueChanged(int value)
{
    ui->baseStats_AttackSpinBox->setValue(value);
}

void MainWindow::on_baseStats_DefenseSlider_valueChanged(int value)
{
    ui->baseStats_DefenseSpinBox->setValue(value);
}

void MainWindow::on_baseStats_SpAttackSlider_valueChanged(int value)
{
    ui->baseStats_SpAttackSpinBox->setValue(value);
}

void MainWindow::on_baseStats_SpDefenseSlider_valueChanged(int value)
{
    ui->baseStats_SpDefenseSpinBox->setValue(value);
}

void MainWindow::on_baseStats_SpeedSlider_valueChanged(int value)
{
    ui->baseStats_SpeedSpinBox->setValue(value);
}

void MainWindow::on_baseStats_TypeCheckBox_clicked(bool checked)
{
    if (checked)
        ui->baseStats_TypeComboBox2->setCurrentIndex(ui->baseStats_TypeComboBox1->currentIndex());
    else
        if (ui->baseStats_TypeComboBox1->currentIndex() == 0)
            ui->baseStats_TypeComboBox2->setCurrentIndex(1);
        else
            ui->baseStats_TypeComboBox2->setCurrentIndex(0);
}

void MainWindow::on_baseStats_TypeComboBox1_currentIndexChanged(int index)
{
    if (index == ui->baseStats_TypeComboBox2->currentIndex())
        ui->baseStats_TypeCheckBox->setChecked(true);
    else
        ui->baseStats_TypeCheckBox->setChecked(false);
}

void MainWindow::on_baseStats_TypeComboBox2_currentIndexChanged(int index)
{
    if (index == ui->baseStats_TypeComboBox1->currentIndex())
        ui->baseStats_TypeCheckBox->setChecked(true);
    else
        ui->baseStats_TypeCheckBox->setChecked(false);
}

void MainWindow::on_baseStats_ItemCheckBox_clicked(bool checked)
{
    if (checked)
    {
        ui->baseStats_ItemComboBox1->setCurrentIndex(0);
        ui->baseStats_ItemComboBox2->setCurrentIndex(0);
    }
    else
    {
        ui->baseStats_ItemComboBox2->setCurrentIndex(1);
        ui->baseStats_ItemComboBox2->setCurrentIndex(1);
    }
}

void MainWindow::on_baseStats_ItemComboBox1_currentIndexChanged(int index)
{
    if (index == 0 && ui->baseStats_ItemComboBox2->currentIndex() == 0)
        ui->baseStats_ItemCheckBox->setChecked(true);
    else
        ui->baseStats_ItemCheckBox->setChecked(false);
}

void MainWindow::on_baseStats_ItemComboBox2_currentIndexChanged(int index)
{
    if (index == 0 && ui->baseStats_ItemComboBox1->currentIndex() == 0)
        ui->baseStats_ItemCheckBox->setChecked(true);
    else
        ui->baseStats_ItemCheckBox->setChecked(false);
}

void MainWindow::on_radioButton_toggled(bool checked)
{
    if (checked)
        qApp->setPalette(this->style()->standardPalette());
}


void MainWindow::on_radioButton_2_toggled(bool checked)
{    
    if (checked)
    {
        QToolTip::setPalette(darkPalette);

        qApp->setPalette(darkPalette);

        QSettings Settings(qApp->applicationDirPath() + "/myapp.ini", QSettings::IniFormat);
        Settings.setValue("Windows/Test1", "data");
        Settings.setValue("Windows/Test2", "data2");

        Settings.setValue("macOS/Test3", "data");
        Settings.setValue("macOS/Test4", "data2");

        Settings.setValue("Linux/Test5", "data");
        Settings.setValue("Linux/Test6", "data2");

        QString path = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);

        verifyChange(path);
    }
}


void MainWindow::on_radioButton_3_toggled(bool checked)
{
    if (checked)
    {themeTimer = new QTimer(this);

        themeTimer->setSingleShot(false);
        themeTimer->setInterval(1000); // 1 seconds
        connect(themeTimer, SIGNAL(timeout()), this, SLOT(themeCheck()));
        themeTimer->start(1000);
    }

    else
        themeTimer->deleteLater();
}

void MainWindow::themeCheck()
{
    QSettings m_settings{
        "HKEY_CURRENT_"
        "USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize",
        QSettings::NativeFormat};

    bool darkTheme = !m_settings.value("AppsUseLightTheme", true).toBool();

    if (darkTheme)
    {
        QToolTip::setPalette(darkPalette);

        qApp->setPalette(darkPalette);
    }
    else
        qApp->setPalette(this->style()->standardPalette());
}

void MainWindow::on_file_OpenButton_clicked()
{
    dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 "/home",
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    if (dir.isNull() == false)
    {
        ui->tabWidget->setCurrentIndex(0);
        ui->pokemon_Container->setCurrentIndex(0);
    }

    ui->tabWidget->setEnabled(true);

    switchBaseState();

    setConstants(dir);

    setupEditor();
}


void MainWindow::on_settings_GotoButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}


void MainWindow::on_settings_ReturnButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

