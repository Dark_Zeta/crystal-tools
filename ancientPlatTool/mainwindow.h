#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QFileSystemModel>
#include <QMessageBox>
#include <highlighter.h>
#include <QListWidgetItem>
#include <QTimer>
#include <QStandardPaths>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_tabWidget_2_currentChanged(int index);

    void pokemon_EntryChange();

    void baseStats_UpdateGUI();

    void baseStats_UpdateText();

    QString readFile(QString filename);

    void on_listWidget_itemChanged(QListWidgetItem *item);

    void on_baseStats_HPSpinBox_valueChanged(int arg1);

    void on_baseStats_AttackSpinBox_valueChanged(int arg1);

    void on_baseStats_DefenseSpinBox_valueChanged(int arg1);

    void on_baseStats_SpAttackSpinBox_valueChanged(int arg1);

    void on_baseStats_SpDefenseSpinBox_valueChanged(int arg1);

    void on_baseStats_SpeedSpinBox_valueChanged(int arg1);

    void on_baseStats_HPSlider_valueChanged(int value);

    void on_baseStats_AttackSlider_valueChanged(int value);

    void on_baseStats_DefenseSlider_valueChanged(int value);

    void on_baseStats_SpAttackSlider_valueChanged(int value);

    void on_baseStats_SpDefenseSlider_valueChanged(int value);

    void on_baseStats_SpeedSlider_valueChanged(int value);

    void on_baseStats_TypeCheckBox_clicked(bool checked);

    void on_baseStats_TypeComboBox1_currentIndexChanged(int index);

    void on_baseStats_TypeComboBox2_currentIndexChanged(int index);

    void on_baseStats_ItemCheckBox_clicked(bool checked);

    void on_baseStats_ItemComboBox1_currentIndexChanged(int index);

    void on_baseStats_ItemComboBox2_currentIndexChanged(int index);

    void on_radioButton_toggled(bool checked);

    void on_radioButton_2_toggled(bool checked);

    void on_radioButton_3_toggled(bool checked);

    void themeCheck();

    void on_file_OpenButton_clicked();

    void on_settings_GotoButton_clicked();

    void on_settings_ReturnButton_clicked();

private:
    Ui::MainWindow *ui;

    void switchBaseState();

    void setupEditor();

    void setConstants(QString);

    QString dir;

    QTimer *themeTimer;

    QFileSystemModel *model = new QFileSystemModel();

    Highlighter *highlighter;

    QStringList keywordPatterns;

    bool verifyChange(QString message);

    int findLine(QString param);

    void funcSlider(int val, QSlider* curSlider);

    QPalette darkPalette;
};
#endif // MAINWINDOW_H
